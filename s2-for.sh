#!/bin/bash

suc=0
fal=0

for teste in $1 $2 $3 $4
do
	ping -c1 $teste &>/dev/null
	test $? -eq 0 && suc=$(($suc + 1)) || fal=$(($fal + 1))
done

printf "Sucessos: $suc\n"
printf "Falhas: $fal\n"
