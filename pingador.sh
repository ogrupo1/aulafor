#!/bin/bash

total=0

for ((i=1;i<=254;i++))
do
	echo 10.0.51.$i
	ping -c1 10.0.51.$i &>/dev/null
	test $? -eq 0 && total=$(($total + 1)) && echo 10.0.51.$i >> arq
done

echo Total de máquinas disponíveis: $total

cat arq

rm arq
